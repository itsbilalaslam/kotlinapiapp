package com.example.kotlinapp

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.kotlinapp.DataModel.Article
import com.example.kotlinapp.DataModel.News
import com.example.kotlinapp.Retrofit.ApiClient
import retrofit2.Call
import retrofit2.Response

object repository {

    var newsList = MutableLiveData<News>()

    fun getNews(country: String, page: Int):MutableLiveData<News>{

        val call = ApiClient.apiInterface.getHeadlines(country, page)

        call.enqueue(object : retrofit2.Callback<News> {
            override fun onResponse(call: Call<News>, response: Response<News>) {


                val news = response.body()

                if (news != null) {

                Log.d("news", "news working")

                    Log.d("author", news.articles.toString())
//                    Log.d("title", news.articles.get(0).title)
                    newsList.value= news
                }
                else{
                    Log.d("news", news.toString())
                }


            }

            override fun onFailure(call: Call<News>, t: Throwable) {
                Log.d("news", "Error in fetching news " + t.message)
//                //Toast.makeText(applicationContext, "Server is not responding " + t.message, Toast.LENGTH_SHORT).show()
            }

        })

        return newsList
    }
}