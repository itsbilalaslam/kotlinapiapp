package com.example.kotlinapp.Adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.kotlinapp.DataModel.Article
import com.example.kotlinapp.R
import com.squareup.picasso.Picasso



class NewsAdapter(val userList: List<Article>, private val listener: OnItemClickListener) : RecyclerView.Adapter<NewsAdapter.ViewHolder>() {


    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_list, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(userList[position])

        Log.d("bindItems", "bindItems call: ")
        holder.itemView.setOnClickListener{
            listener.onItemClick(userList[position])
        }
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return userList.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        fun bindItems(news: Article) {
            val textViewAuthor = itemView.findViewById(R.id.author) as TextView
            val textViewTitle  = itemView.findViewById(R.id.title) as TextView
            val textViewDes  = itemView.findViewById(R.id.des) as TextView
            val ImgView  = itemView.findViewById(R.id.image) as ImageView

            textViewAuthor.text = news.author
            textViewTitle.text = news.title
            textViewDes.text = news.description


            Picasso.get()
                    .load(news.urlToImage)
                    .placeholder(R.drawable.progress_animation)
                    .resize(250, 250)
                    .centerCrop()
                    .into(ImgView)
        }
    }

    interface OnItemClickListener {
        fun onItemClick(news: Article)
    }



}