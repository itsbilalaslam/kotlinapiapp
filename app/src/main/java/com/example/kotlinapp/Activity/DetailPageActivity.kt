package com.example.kotlinapp.Activity

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.example.kotlinapp.DataModel.Article
import com.example.kotlinapp.R
import com.google.gson.Gson
import com.squareup.picasso.Picasso


class DetailPageActivity : AppCompatActivity() {

    lateinit var articleObject: Article
    lateinit var buttonUrl: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_page)

        val gson = Gson()
        articleObject = gson.fromJson(intent.getStringExtra("newsObject"), Article::class.java)

        initilize(articleObject)
        clickListener()

    }

    private fun clickListener() {

        buttonUrl.setOnClickListener(View.OnClickListener {

            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(articleObject.url))
            startActivity(browserIntent)
        })

    }

    private fun initilize(article: Article) {
        val textViewAuthor = findViewById<TextView>(R.id.author)
        val textViewTitle  = findViewById<TextView>(R.id.title)
        val textViewDes  = findViewById<TextView>(R.id.des)
        val textViewPublish  = findViewById<TextView>(R.id.publish_tv)
        val textViewPublishHeader  = findViewById<TextView>(R.id.publish_header_tv)
        val textViewContentHeader  = findViewById<TextView>(R.id.content_header_tv)
        val textViewContent  = findViewById<TextView>(R.id.content_tv)
        buttonUrl  = findViewById(R.id.url_btn) as Button
        val ImgView  =    findViewById(R.id.image) as ImageView

        textViewAuthor.text = article.author
        textViewTitle.text = article.title
        textViewDes.text = article.description


//            if(article.publishedAt.isEmpty() || article.content.isEmpty() ){
//            textViewPublishHeader.visibility= View.GONE
//            textViewContentHeader.visibility= View.GONE
//            }
//            else{
//                textViewContent.text = article.content
//                textViewPublish.text = article.publishedAt
//            }

        textViewContent.text = article.content
        textViewPublish.text = article.publishedAt
        Picasso.get()
                .load(article.urlToImage)
                .placeholder(R.drawable.progress_animation)
                .resize(300, 300)
                .centerCrop()
                .into(ImgView)



    }


}