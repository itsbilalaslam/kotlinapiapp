package com.example.kotlinapp.Activity

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.example.kotlinapp.DataModel.Main2
import com.example.kotlinapp.R
import com.example.kotlinapp.Retrofit.WeatherService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomeActivity : AppCompatActivity() {

    lateinit var newsBtn: Button
    lateinit var weatherBtn: Button
    lateinit var cityEt: EditText
    lateinit var tempTv: TextView
    var flag=false

    val api_key="db392cb4b460277092d1a34dcc9bdfcf"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)



        initilize()
        clickListener()

        weatherBtn= findViewById<Button>(R.id.weather)
        weatherBtn.setOnClickListener(View.OnClickListener {

            val weather= WeatherService.weatherInstance.getHeadlines(cityEt.text.toString().trim(),api_key)

            weather.enqueue(object : Callback<Main2> {
                override fun onResponse(call: Call<Main2>, response: Response<Main2>) {

                    val weather=response.body()

                    if(weather!=null){
                        Log.d("weather", weather.main?.temp.toString())

                        tempTv.text=weather.main?.temp.toString()
                    }
                }

                override fun onFailure(call: Call<Main2>, t: Throwable) {
                    Log.d("weather", "error in weather fetching "+t.message)
                }

            })

        })


    }

    private fun clickListener() {
        newsBtn.setOnClickListener(View.OnClickListener {
            checkConnection()
            if(flag){
                val intent = Intent(this, APIActivity::class.java)
                startActivity(intent)
            }
        })
    }

    private fun initilize() {
        cityEt= findViewById(R.id.city_et)
        tempTv= findViewById(R.id.temp)
        newsBtn= findViewById(R.id.news)
    }

    private fun checkConnection() {
          val manager=applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
          val networkInfo=manager.activeNetworkInfo
          if(null!=networkInfo){
              if(networkInfo.type== ConnectivityManager.TYPE_WIFI || networkInfo.type== ConnectivityManager.TYPE_WIFI ){

                  Toast.makeText(applicationContext,"Internet is Connected",Toast.LENGTH_SHORT).show()
                  flag=true
              }
          }
          else{
              Toast.makeText(applicationContext,"Connect the Internet",Toast.LENGTH_SHORT).show()
              flag=false
          }

    }


}