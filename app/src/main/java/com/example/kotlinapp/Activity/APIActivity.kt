package com.example.kotlinapp.Activity


import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout.OnRefreshListener
import com.example.kotlinapp.Adapter.NewsAdapter
import com.example.kotlinapp.ApiViewModel
import com.example.kotlinapp.DataModel.Article
import com.example.kotlinapp.DataModel.News
import com.example.kotlinapp.R
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Response
import java.util.*


class APIActivity : AppCompatActivity(), NewsAdapter.OnItemClickListener{

    lateinit var recyclerView: RecyclerView
    lateinit var newsList: List<Article>
    lateinit  var swipeRefreshLayout: SwipeRefreshLayout

    lateinit var adapter: NewsAdapter
    lateinit var layoutManager: LinearLayoutManager
    lateinit var progressBr: ProgressBar

    lateinit var mainActivityViewModel: ApiViewModel

    var page = 0
    var isLoading = false
    val limit = 10

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_api)

        initialize()

       getNews(2)
      //  getPage()
        clickListener()

    }

    private fun clickListener() {
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {


                val visibleItemCount = layoutManager.childCount
                val pastVisibleItem = layoutManager.findFirstCompletelyVisibleItemPosition()
                val total = adapter.itemCount

                if (!isLoading) {

                    if ((visibleItemCount + pastVisibleItem) >= total) {
                        page++
                        getPage()
                    }

                }
                super.onScrolled(recyclerView, dx, dy)
            }
        })

        swipeRefreshLayout.setOnRefreshListener(OnRefreshListener {
            val page= 1
            getNews(page)
            swipeRefreshLayout.setRefreshing(false)
        })


    }

    fun getPage() {
        isLoading = true
        val start = ((page) * limit) + 1
        val end = (page + 1) * limit

        for (i in start..end) {
            getNews(start)
        }

        Handler().postDelayed({
            if (::adapter.isInitialized) {
                adapter.notifyDataSetChanged()
            } else {
                adapter = NewsAdapter(newsList, this)
                recyclerView.adapter = adapter
            }
            isLoading = false

        }, 5000)

    }

   fun initialize(){

       recyclerView = findViewById(R.id.recycler) as RecyclerView
      // recyclerView.setLayoutManager(LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false))
       layoutManager = LinearLayoutManager(this)
       recyclerView.layoutManager = layoutManager

       newsList=ArrayList()

       progressBr= findViewById(R.id.progressbar_api)
       swipeRefreshLayout= findViewById(R.id.swipeRefresh)

       mainActivityViewModel = ViewModelProvider(this).get(ApiViewModel::class.java)

   }

    private fun getNews(page: Int) {
        progressBr.visibility= View.VISIBLE

        Log.d("Mian", "inside getNews Mian")
        mainActivityViewModel.getNews("us", page)!!.observe(this, { news ->


            Log.d("mainact", news.articles.toString())
            newsList = news.articles
            val adapter = NewsAdapter(newsList, this@APIActivity)
            recyclerView.adapter = adapter
        })
//        val news= NewsService.newsInstance.getHeadlines("us", page)
//        Log.d("page", page.toString())
//        news.enqueue(object : retrofit2.Callback<News> {
//            override fun onFailure(call: Call<News>, t: Throwable) {
//                Log.d("news", "Error in fetching news " + t.message)
//                Toast.makeText(applicationContext, "Server is not responding " + t.message, Toast.LENGTH_SHORT).show()
//            }
//
//            override fun onResponse(call: Call<News>, response: Response<News>) {
//
//                val news = response.body()
//
//                if (news != null) {
//
//                    Log.d("news", news.toString())
//
//                    newsList = news.articles
////                    val adapter = CustomAdapter(newsList, this@APIActivity)
////                    recyclerView.adapter = adapter
//                } else {
//                    Toast.makeText(applicationContext, "No Data found", Toast.LENGTH_SHORT).show()
//                }
//                progressBr.visibility = View.GONE
//            }
//        })
    }


    override fun onItemClick(news: Article) {
        val intent = Intent(this, DetailPageActivity::class.java)
        val gson = Gson()
        val myJson = gson.toJson(news)
        intent.putExtra("newsObject", myJson)
        startActivity(intent)
        startActivity(intent)
    }



}