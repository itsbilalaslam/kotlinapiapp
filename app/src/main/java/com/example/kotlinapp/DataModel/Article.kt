package com.example.kotlinapp.DataModel

data class Article(val author: String,val title: String, val description: String,val urlToImage: String,
                   val publishedAt: String, val url: String,val content: String,)