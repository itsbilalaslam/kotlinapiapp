package com.example.kotlinapp.DataModel

data class News (val totalResults: Int, val articles: List<Article>)