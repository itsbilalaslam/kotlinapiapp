package com.example.kotlinapp

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.kotlinapp.DataModel.Article
import com.example.kotlinapp.DataModel.News

class ApiViewModel : ViewModel()  {

    var servicesLiveData: MutableLiveData<News>? = null

    fun getNews(c:String,p:Int) : LiveData<News>? {

        servicesLiveData = repository.getNews(c,p)
        return servicesLiveData
    }

}