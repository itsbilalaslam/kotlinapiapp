package com.example.kotlinapp.Retrofit

import com.example.kotlinapp.DataModel.News
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

const val BaseUrl="https://newsapi.org/"
const val ApiKey="dcd32c47c12c443ea4d889eab8b4659d"

interface ApiInterface {

    @GET("v2/top-headlines?apiKey=$ApiKey")
    fun getHeadlines(@Query("country")country: String, @Query("page")  page: Int): Call <News>

}
//
////singleton like static
//object NewsService{
//    val newsInstance: ApiInterface
//
//    init {
//        val retrofit=Retrofit.Builder()
//                .baseUrl(BaseUrl)
//                .addConverterFactory(GsonConverterFactory.create())
//                .build()
//        newsInstance=retrofit.create(ApiInterface::class.java)
//    }
//}