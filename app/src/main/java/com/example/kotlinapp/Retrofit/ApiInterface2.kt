package com.example.kotlinapp.Retrofit

import com.example.kotlinapp.DataModel.Main2
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

const val base_url ="https://api.openweathermap.org/data/2.5/"
       // "weather?q={city name}&appid={API key}"
const val api_key="db392cb4b460277092d1a34dcc9bdfcf"
interface ApiInterface2
{
    @GET("weather")
    fun getHeadlines(@Query("q")city: String,
                     @Query("appid")  appid: String): Call<Main2>

}

//singleton like static
object WeatherService{
    val weatherInstance: ApiInterface2

    init {
        val retrofit= Retrofit.Builder()
                .baseUrl(base_url)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        weatherInstance=retrofit.create(ApiInterface2::class.java)
    }
}